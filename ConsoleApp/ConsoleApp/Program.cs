﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    // Класс для создания Контактов
    class Contacts
    {
        public string surname;
        public string name;
        public string patronymic;
        public string number;
        public string country;
        public string birthday;
        public string organization;
        public string post;
        public string other;

        // Коструктор класса Contacts
        public Contacts(string s, string n, string pat, string num, string c, string b, string org, string p, string o)
        {
            surname = s;
            name = n;
            patronymic = pat;
            number = num;
            country = c;
            birthday = b;
            organization = org;
            post = p;
            other = o;
        }
    }

    // Класс для создания Телефонной книги
    class PhoneBook
    {
        List<Contacts> contacts;

        // Конструктор класса PhoneBook
        public PhoneBook()
        {
            contacts = new List<Contacts>();
        }

        // Функция для добавления контакта
        public void add(string s, string n, string pat, string num, string c, string b, string org, string p, string o)
        {
            Contacts date = new Contacts(s, n, pat, num, c, b, org, p, o);
            contacts.Add(date);
        }

        // Функция для возвращения контакта
        public bool remove(string name, string surname)
        {
            Contacts date = find(name, surname);

            if (date != null)
            {
                contacts.Remove(date);
                return true;
            }
            else return false;
        }

        public void list(Action<Contacts> action)
        {
            contacts.ForEach(action);
        }

        public bool isEmpty()
        {
            return (contacts.Count == 0);
        }

        // Функция для поиска контакта по имени(name) и фамилии(surname)
        public Contacts find(string name, string surname)
        {
            Contacts date = contacts.Find(
              delegate (Contacts a)
              {
                  return (a.name == name && a.surname == surname);
              }
            );
            return date;
        }
    }

    // Класс для создания рабочего приложения
    class PhonePrompt
    {
        PhoneBook book;

        // Конструктор класса PhonePrompt
        public PhonePrompt()
        {
            book = new PhoneBook();
        }

        // Главная функция
        static void Main(string[] args)
        {
            string selection = "";
            PhonePrompt prompt = new PhonePrompt();

            while (!selection.ToUpper().Equals("Q"))
            {
                Console.Clear();
                prompt.displayMenu();
                Console.Write("Selection: ");
                selection = Console.ReadLine();
                prompt.performAction(selection);
            }
        }

        // Функция вывода меню выбора
        void displayMenu()
        {
            Console.WriteLine("Main Menu");
            Console.WriteLine("---------");
            Console.WriteLine("1 - Add an Contact");
            Console.WriteLine("2 - Edit an Contact");
            Console.WriteLine("3 - Delete an Contact");
            Console.WriteLine("4 - List Contact");
            Console.WriteLine("5 - List All Contacts");
            Console.WriteLine("---------");
            Console.WriteLine("Q - Quit\n");
        }

        // Функция вывода меню выбора
        void two_displayMenu()
        {
            Console.WriteLine("Menu choose");
            Console.WriteLine("---------");
            Console.WriteLine("1 - Surname");
            Console.WriteLine("2 - Name");
            Console.WriteLine("3 - Patronymic");
            Console.WriteLine("4 - Number");
            Console.WriteLine("5 - Country");
            Console.WriteLine("6 - Birthday");
            Console.WriteLine("7 - Organization");
            Console.WriteLine("8 - Post");
            Console.WriteLine("9 - Other");
            Console.WriteLine("---------");
            Console.WriteLine("B - Back\n");
        }

        // Функция взаимодействия с пользователем
        void performAction(string selection)
        {
            string surname = "";
            string name = "";
            string patronymic = "";
            string number = "";
            string country = "";
            string birthday = "";
            string organization = "";
            string post = "";
            string other = "";

            switch (selection.ToUpper())
            {
                case "1": // Добавление контактов
                    Console.Clear();
                    Console.WriteLine("\n* - obligatory filling\n");

                    // Фамилия
                    Console.Write("Enter Surname*: ");
                    surname = Console.ReadLine();

                    while (surname == "")
                    {
                        Console.Write("Warning!!! Enter Surname, please: ");
                        surname = Console.ReadLine();
                        Console.Write("\n");
                    }

                    // Имя
                    Console.Write("Enter Name*: ");
                    name = Console.ReadLine();

                    while (name == "")
                    {
                        Console.Write("Warning!!! Enter Name, please: ");
                        name = Console.ReadLine();
                        Console.Write("\n");
                    }

                    // Отчество
                    Console.Write("Enter Patronymic: ");
                    patronymic = Console.ReadLine();

                    // Телефон
                    Console.Write("Enter Number(only digit)*: ");
                    long result;
                    number = Console.ReadLine();

                    while (number == "")
                    {
                        Console.Write("Warning!!! Enter Number, please: ");
                        number = Console.ReadLine();
                        Console.Write("\n");
                    }
                    result = Convert.ToInt64(number);

                    // Страна
                    Console.Write("Enter Country*: ");
                    country = Console.ReadLine();

                    while (country == "")
                    {
                        Console.Write("Warning!!! Enter Country, please: ");
                        country = Console.ReadLine();
                        Console.Write("\n");
                    }

                    // Дата рождения
                    Console.Write("Enter Birthday: ");
                    birthday = Console.ReadLine();

                    // Организация
                    Console.Write("Enter Organization: ");
                    organization = Console.ReadLine();

                    // Должность
                    Console.Write("Enter Post: ");
                    post = Console.ReadLine();

                    // Прочии заметки
                    Console.Write("Enter Other: ");
                    other = Console.ReadLine();

                    // Создание контакта
                    book.add(surname, name, patronymic, number, country, birthday, organization, post, other);

                    Console.WriteLine("\nContact successfully added!\n");
                    Console.WriteLine("Press any key now to continue...");
                    Console.ReadKey();
                    break;

                case "2": // Редактирование
                    Console.Clear();

                    Console.Write("Enter Surname to Edit: ");
                    surname = Console.ReadLine();

                    Console.Write("Enter Name to Edit: ");
                    name = Console.ReadLine();

                    Contacts date = book.find(name, surname);

                    if (date == null)
                    {
                        Console.WriteLine("\nContact for {0} {1} count not be found...\n", surname, name);
                        Console.WriteLine("Press any key now to continue...");
                        Console.ReadKey();
                    }

                    else
                    {
                        string edition = "";
                        PhonePrompt edit = new PhonePrompt();

                        while (!edition.ToUpper().Equals("B"))
                        {
                            Console.Clear();
                            edit.two_displayMenu();
                            Console.Write("Edition: ");
                            edition = Console.ReadLine();

                            switch (edition)
                            {
                                case "1": // Фамилия
                                    Console.Write("\nEnter new Surname: ");
                                    date.surname = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;

                                case "2": // Имя
                                    Console.Write("\nEnter new Name: ");
                                    date.name = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;

                                case "3": // Отчество
                                    Console.Write("\nEnter new Patronymic: ");
                                    date.patronymic = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    break;

                                case "4": // Телефон
                                    Console.Write("\nEnter new Number: ");
                                    date.number = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;

                                case "5": // Страна
                                    Console.Write("\nEnter new Country: ");
                                    date.country = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;

                                case "6": // Дата рождения
                                    Console.Write("\nEnter new Birthday: ");
                                    date.birthday = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;

                                case "7": // Организация
                                    Console.Write("\nEnter new Organization: ");
                                    date.organization = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;

                                case "8": // Должность
                                    Console.Write("\nEnter new Post: ");
                                    date.post = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;

                                case "9": // Прочии заметки
                                    Console.Write("\nEnter new Other: ");
                                    date.other = Console.ReadLine();

                                    Console.WriteLine("\nContact updated for {0} {1}!\n", surname, name);
                                    Console.WriteLine("Press any key now to continue...");
                                    Console.ReadKey();
                                    break;
                            }
                        }
                    }
                    break;

                case "3": // Удаление
                    Console.Write("\nEnter Surname to Delete: ");
                    surname = Console.ReadLine();

                    Console.Write("Enter Name to Delete: ");
                    name = Console.ReadLine();

                    if (book.remove(name, surname))
                    {
                        Console.WriteLine("\nContact successfully removed!\n");
                        Console.WriteLine("Press any key now to continue...");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("\nContact for {0} {1} could not be found\n", surname, name);
                        Console.WriteLine("Press any key now to continue...");
                        Console.ReadKey();
                    }
                    break;

                case "4": // Просмотр контакта
                    Console.Write("\nEnter Surname to Found: ");
                    surname = Console.ReadLine();

                    Console.Write("Enter Name to Found: ");
                    name = Console.ReadLine();

                    Contacts find = book.find(name, surname);

                    if (find == null)
                    {
                        Console.WriteLine("\nContact for {0} {1} count not be found\n", surname, name);
                        Console.WriteLine("Press any key now to continue...");
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.Clear();

                        Console.WriteLine("Contact:\n");
                        Console.WriteLine("Surname - {0}", find.surname);
                        Console.WriteLine("Name - {0}", find.name);
                        Console.WriteLine("Patronymic - {0}", find.patronymic);
                        Console.WriteLine("Number - {0}", find.number);
                        Console.WriteLine("Country - {0}", find.country);
                        Console.WriteLine("Birthday - {0}", find.birthday);
                        Console.WriteLine("Organization - {0}", find.organization);
                        Console.WriteLine("Post - {0}", find.post);
                        Console.WriteLine("Other - {0}\n", find.other);

                        Console.WriteLine("Press any key now to continue...");
                        Console.ReadKey();
                    }
                    break;

                case "5": // Просмотр всех контактов
                    if (book.isEmpty()) Console.WriteLine("There are no entries\n");
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Contacts:\n");
                        Console.WriteLine("Surname - Name - Number\n");

                        book.list(
                            delegate (Contacts a)
                            {
                                Console.WriteLine("{0} - {1} - {2}\n", a.surname, a.name, a.number);
                            }
                        );
                    }

                    Console.WriteLine("Press any key now to continue...");
                    Console.ReadKey();
                    break;
            }
        }
    }
}